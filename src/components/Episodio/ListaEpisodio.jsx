import { Episodio } from './Episodio';

export function ListaEpisodio({ episodios }) {
  return (
    <div className='row'>
      {episodios.map((episodio) => (
        <Episodio key={episodio.id} {...episodio.data} />
      ))}
    </div>
  );
}
