export function Footer() {
  return (
    <div className='navbar-dark bg-dark fixed-bottom'>
      <p className='text-center text-white py-3 mb-0'>
        Proyecto creado por Angelica Morales - CAR IV 2022
      </p>
    </div>
  );
}
