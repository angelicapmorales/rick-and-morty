import { PersonajeItem } from './PersonajeItem';
import { useEffect, useState } from 'react';
import axios from 'axios';

export function ListadoPersonajes({ buscar }) {
  const [personajes, setPersonajes] = useState(null);
  let personajesFiltrados = personajes?.results;
  if (buscar && personajes) {
    personajesFiltrados = personajes.results.filter((personaje) => {
      let nombrePrsonajeMinuscula = personaje.name.toLowerCase();
      let buscadorMinuscula = buscar.toLowerCase();
      return nombrePrsonajeMinuscula.includes(buscadorMinuscula);
    });
  }

  useEffect(() => {
    axios.get('https://rickandmortyapi.com/api/character').then((respuesta) => {
      setPersonajes(respuesta.data);
    });
  }, []);

  return (
    <div className='row py-5'>
      {personajes
        ? personajesFiltrados.map((elemento) => {
            return <PersonajeItem key={elemento.id} {...elemento} />;
          })
        : 'Cargando...'}
    </div>
  );
}
