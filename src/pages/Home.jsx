import { Buscador } from '../components/Buscador/Buscador';
import { Banner } from '../components/Navbar/Banner';
import { ListadoPersonajes } from '../components/Personajes/ListadoPersonajes';
import { useState } from 'react';

export function Home() {
  let [buscador, setBuscador] = useState('');
  return (
    <div>
      <Banner />
      <div className='px-5'>
        <h1 className='py-3'>Personajes destacados</h1>

        <Buscador valor={buscador} onBuscar={setBuscador} />
        <ListadoPersonajes buscar={buscador} />
      </div>
    </div>
  );
}
